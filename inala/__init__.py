"""
All modules for running a Inala Discord bot.
"""

import logging

from .commands import parse_command, upload_emojis, emojis, update_message
from .util import get_discord_id, preprocess_input
from .database import load_settings, get_guild_settings, get_guild_settings


logger = logging.getLogger(__name__)
logging.basicConfig()
logger.setLevel(logging.DEBUG)

load_settings()
