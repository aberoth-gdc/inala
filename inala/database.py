"""
Storage for Inala
"""

import json
import logging
import pickle
import sys
import typing


logger = logging.getLogger(__name__)
logging.basicConfig()
logger.setLevel(logging.DEBUG)

# this is a pointer to the module object instance itself.
this = sys.modules[__name__]

guild_settings: dict[int, dict[str, typing.Any]] = {}


def save_settings() -> None:
    _save_settings_pickle()


def _save_settings_pickle() -> None:
    pickle.dump(guild_settings, open("data/guild_settings.p", "wb"))


def _save_settings_redis() -> None:
    raise NotImplementedError


def load_settings() -> None:
    _load_settings_pickle()


def _load_settings_pickle() -> None:
    this.guild_settings = pickle.load(open("data/guild_settings.p", "rb"))
    logger.debug(json.dumps(guild_settings, indent=2))


def _load_settings_redis() -> None:
    raise NotImplementedError


def get_guild_settings(guild_id: int) -> dict[str, typing.Any]:
    return guild_settings[guild_id]
