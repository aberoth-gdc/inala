import json
import logging
import os
from os import listdir
from os.path import isfile, join
import re
import typing

import discord

from .util import get_discord_id
from .database import guild_settings, save_settings

logger = logging.getLogger(__name__)
logging.basicConfig()
logger.setLevel(logging.DEBUG)

sub_re_realm_colors = "(white|black|green|red|purple|yellow|cyan|blue)"
sub_re_realm_names = "(knowledge|power|thought|life|change|creation|protection|deceit)"
sub_re_realm = f"(arc(ane)? |common )?(realm (of )?)?({sub_re_realm_colors}|{sub_re_realm_names})"

re_start_camp = re.compile('[A-z ]*camp.*')
re_start_mark = re.compile(f'[A-z ]* (arc )?(?P<realm>{sub_re_realm}) marks.*')
re_gen_user = re.compile('generate (a )?(new )? user( (on )? (realm)).*')

assert re_start_camp.match('start camps')

trivia: dict[str, str] = {
}

emojis = [f for f in listdir('resources/') if isfile(join('resources/', f))]


def parse_command(message) -> typing.Callable[[typing.Any], str]:
    if len(message) ==  1:
        return greeting
    if len(message) == 2 and content[1].lower() in trivia:
        return get_trivia
    if re_start_camp.match(" ".join(message[1:])):
        return start_camp
    if re_start_mark.match(" ".join(message[1:])):
        return start_mark

    return no_match


async def no_match(message) -> str:
    no_match_message: str = "Greetings, {}. I do not know anything about that."
    return no_match_message.format(message.author.mention)


async def greeting(message) -> str:
    # They only said our name.
    return "Greetings, {}. How can I help?".format(message.author.mention)


async def get_trivia(message) -> str:
    content = 
    return trivia[content[1].lower()].format(message.author.mention)


async def generate_new_user(message) -> str:
    raise NotImplementedError


async def start_camp(message) -> str:
    """
    Posts a message to the channel and begins monitoring for reactions.
    """

    title: str = "React to this message to mark camps!".format('')
    url: str = "https://tavelors.pub"
    description: str = "These camps will automatically be removed after one hour."

    embed: discord.Embed = discord.Embed(
        title=title,
        url=url,
        description=description,
    )

    embed.set_author(name="Yellow Realm Camps")
    embed.set_footer(text="sadf", image="https://liberapay.com/assets/widgets/donate.svg")

    msg = await message.channel.send(embed=embed)

    emojis = guild_settings[msg.guild.id]['emojis']

    await msg.add_reaction(emojis['malch'])
    await msg.add_reaction(emojis['ourik'])
    await msg.add_reaction(emojis['gram'])
    await msg.add_reaction(emojis['ww'])
    await msg.add_reaction(emojis['forstyll'])
    await msg.add_reaction(emojis['prat'])
    await msg.add_reaction(emojis['mt'])
    await msg.add_reaction(emojis['mino'])
    await msg.add_reaction(emojis['skald'])
    await msg.add_reaction(emojis['lich'])

    return ""


async def start_mark(message) -> str:
    """
    Posts a message to the channel and begins monitoring for reactions.
    """

    msg: str = "React with what you've killed recently!"

    await message.channel.send(msg)


def create_emoji_dict(emoji_list: list[discord.Emoji]) -> dict[str, discord.Emoji]:
    result: dict[str, discord.Emoji] = {}
    for emoji in emoji_list:
        result[emoji.name] = emoji

    return result


async def upload_emojis(guild) -> None:
    reason: str = "Uploaded by Inala bot"

    logger.info('asdf')
    logger.info(json.dumps(guild_settings, indent=2))

    if guild.id not in guild_settings:
        logger.debug("Guild {} does not have settings.".format(guild.name))
        guild_settings[guild.id] = {}

    logger.debug(json.dumps(guild_settings[guild.id], indent=2))

    if 'emojis' in guild_settings[guild.id]:
        return

    guild_settings[guild.id]['emojis'] = {}

    existing_emojis = await guild.fetch_emojis()
    existing_emojis_dict = create_emoji_dict(existing_emojis)

    for emoji in emojis:
        if emoji not in existing_emojis:
            logger.info("Uploading %s" % os.path.join('resources/', emoji))
            with open(os.path.join('resources/', emoji), 'rb') as f:
                result = await guild.create_custom_emoji(
                    name=emoji,
                    image=f.read(),
                    reason=reason,
                )
        else:
            result = existing_emojis[emoji]

        guild_settings[guild.id]['emojis'][emoji] = str(result)

    save_settings()


def update_message(
    message: discord.Message,
    emoji: discord.PartialEmoji,
    member: discord.Member
) -> None:
    raise NotImplementedError
