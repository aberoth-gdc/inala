from setuptools import setup


def readme():
    """
    Function to read the long description for the Inala package.
    """
    with open('README.md') as _file:
        return _file.read()


setup(
    name='inala',
    version='0.1.0',
    description="Library for launching a Inala bot",
    long_description=readme(),
    long_description_content_type='text/markdown',
    url='https://gitlab.com/aberoth-gdc/inala',
    author='Twoshields',
    classifiers=[
        "Topic :: Software Development :: Libraries :: Python Modules"
    ],
    packages=['inala'],
    install_requires=['discord', 'redis', 'beautifulsoup4'],
    python_requires='>=3.9',
    zip_safe=False)
