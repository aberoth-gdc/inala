#!/usr/bin/python

import json
import logging
import os

import discord
from discord.errors import Forbidden
import redis

import inala


logger = logging.getLogger(__name__)
logging.basicConfig()
logger.setLevel(logging.DEBUG)

TOKEN: str = os.getenv('DISC_BOT_TOKEN')
logger.info(f"Discord token: {TOKEN}")

client = discord.Client()

#REDIS_HOST: str = os.getenv("REDIS_HOST")
#REDIS_PORT: str = os.getenv("REDIS_PORT")
#logger.info(f"Redis: {REDIS_HOST}:{REDIS_PORT}")

REDIS_URL: str = os.getenv("REDISCLOUD_URL")

redis = redis.Redis(REDIS_URL)


@client.event
async def on_ready():
    """
    TODO
    """
    logger.info("===== INALA BEGIN =====")

    logger.info("INALA IS READY")
    logger.info("INALA USER ID: {}".format(client.user.id))

    return


@client.event
async def on_message(message):
    logger.info("{}, #{}, {}: {!r}".format(message.guild, message.channel, message.author, message.content))

    # Don't bother with Inala's messages
    if message.author == client.user:
        return

    if len(message.content) == 0:
        return

    # handle when we're not mentioned
    if inala.get_discord_id(message.content.split()[0]) != client.user.id:
        return

    cmd = inala.parse_command(message)
    msg: str = await cmd(message)

    if msg != "":
        logger.debug(msg)
        # Try to do a fancy reply, otherwise just send to channel.
        try:
            await message.reply(msg)
        except Forbidden:
            await message.channel.send(msg)


@client.event
async def on_raw_reaction_add(payload):
    # If it's an emoji we don't use, ignore
	logger.debug("Emoji Name: %s", payload.emoji.name)
    if payload.emoji.name not in inala.emojis:
        logger.debug(json.dumps(inala.emojis))
        logger.debug(payload.emoji.name)
        return

    if payload.event_type != "REACTION_ADD":
        return

    # If we are the ones reacting, ignore
    if payload.member.id == client.user.id:
        return

    channel = await client.fetch_channel(payload.channel_id)
    message = await channel.fetch_message(payload.message_id)

    # If we didn't write the message, ignore it.
    if message.author.id != client.user.id:
        return

    inala.update_message(message, payload.emoji, payload.member)

    await message.remove_reaction(payload.emoji, payload.member)


client.run(TOKEN)
